package com.developer.techies.wundartest;


import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.developer.techies.wundartest.adapter.CarInfoAdapter;
import com.developer.techies.wundartest.adapter.holder.CarInfoHolder;
import com.developer.techies.wundartest.model.PlaceMarkModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import static org.assertj.android.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.assertj.android.recyclerview.v7.api.Assertions.assertThat;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class CarInfoAdapterTest {

    private Context context;

    @Before
    public void setUp(){
        context = RuntimeEnvironment.application;
    }

    @Test
    public void carInfoAdapterRecyclerViewTest(){
        List<PlaceMarkModel> carModels = Arrays.asList(
                new PlaceMarkModel("Location : Lesserstraße 170, 22049 Hamburg", "HH-GO8522"),
                new PlaceMarkModel("Lesserstraße 170, 22049 Hamburg", "HH-GO8480")
        );


        CarInfoAdapter adapter = new CarInfoAdapter(carModels);

        RecyclerView recyclerView = new RecyclerView(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        CarInfoHolder holder = adapter.onCreateViewHolder(recyclerView, 0);
        adapter.onBindViewHolder(holder, 0);

        assertEquals(View.VISIBLE, holder.carAddressTv.getVisibility());

        // ToDo Different test assertion
        assertThat(holder.carAddressTv).isVisible();
        adapter.onBindViewHolder(holder, 1);

        assertEquals("Location : Lesserstraße 170, 22049 Hamburg", holder.carAddressTv.getText().toString());

        assertThat(holder.carAddressTv).isVisible().containsText("Hamburg");

        assertThat(adapter).hasItemCount(2);
    }
}
