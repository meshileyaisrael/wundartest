package com.developer.techies.wundartest.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.developer.techies.wundartest.R;


public class CarInfoHolder extends RecyclerView.ViewHolder {

    public TextView carNameTv;
    public TextView carAddressTv;
    public TextView carEngineTypeTv;
    public TextView carExteriorTv;
    public TextView carFuelTv;
    public TextView carInteriorTv;
    public TextView carVinTv;

    public CarInfoHolder(View itemView) {
        super(itemView);
        carNameTv = itemView.findViewById(R.id.car_name);
        carAddressTv = itemView.findViewById(R.id.car_location);
        carEngineTypeTv = itemView.findViewById(R.id.car_engine_type);
        carExteriorTv = itemView.findViewById(R.id.car_exterior);
        carFuelTv = itemView.findViewById(R.id.car_fuel);
        carInteriorTv = itemView.findViewById(R.id.car_interior);
        carVinTv = itemView.findViewById(R.id.car_vin);
    }

}
