package com.developer.techies.wundartest.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.developer.techies.wundartest.model.CarModel;
import com.developer.techies.wundartest.view.fragment.CarsInformationFragment;
import com.developer.techies.wundartest.view.fragment.MapInformationFragment;

public class MainMenuAdapter extends FragmentStatePagerAdapter {

    CarModel carModel;

    public MainMenuAdapter(FragmentManager manager) {
        super(manager);
    }

    public static MainMenuAdapter newInstance(FragmentManager manager, CarModel carModel) {
        MainMenuAdapter adapter = new MainMenuAdapter(manager);
        adapter.setCarModel(carModel);
        return adapter;
    }

    public void setCarModel(CarModel carModel){
        this.carModel = carModel;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return  CarsInformationFragment.newInstance(carModel);
            case 1:
                return  MapInformationFragment.newInstance(carModel);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Car Information";
            case 1:
                return "Map Information";
        }
        return "";
    }
}