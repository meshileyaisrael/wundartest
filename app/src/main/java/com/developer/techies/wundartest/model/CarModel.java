
package com.developer.techies.wundartest.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarModel {

    @SerializedName("placemarks")
    @Expose
    private List<PlaceMarkModel> placemarks = null;

    public List<PlaceMarkModel> getPlacemarks() {
        return placemarks;
    }

    public void setPlacemarks(List<PlaceMarkModel> placemarks) {
        this.placemarks = placemarks;
    }

}
