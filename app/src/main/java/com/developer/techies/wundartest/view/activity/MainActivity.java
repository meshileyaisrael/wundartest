package com.developer.techies.wundartest.view.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import com.developer.techies.wundartest.R;
import com.developer.techies.wundartest.adapter.MainMenuAdapter;
import com.developer.techies.wundartest.model.CarModel;
import com.developer.techies.wundartest.utils.remote.Service;
import com.developer.techies.wundartest.utils.ApiUtils;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    TabLayout mTabLayout;
    ViewPager mViewPager;

    MainMenuAdapter adapter;
    Service mService;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void initView(){
        mService = ApiUtils.getSOService();
        mTabLayout = findViewById(R.id.tabLayout);
        mViewPager = findViewById(R.id.viewPager);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        loadCars();
    }

    private void initData(CarModel carModel) {
        adapter =  MainMenuAdapter.newInstance(getSupportFragmentManager(), carModel);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        progressDialog.dismiss();
    }

    public void loadCars() {
        mService.getCarList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CarModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CarModel carModel) {
                        initData(carModel);
                    }
                });
    }
}
