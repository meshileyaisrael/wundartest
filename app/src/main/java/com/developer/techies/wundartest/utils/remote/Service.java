package com.developer.techies.wundartest.utils.remote;

import com.developer.techies.wundartest.model.CarModel;

import retrofit2.http.GET;
import rx.Observable;

public interface Service {

    @GET("/carsList.json")
    Observable<CarModel> getCarList();
}
