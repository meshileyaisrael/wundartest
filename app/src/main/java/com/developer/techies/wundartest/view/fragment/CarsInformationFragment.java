package com.developer.techies.wundartest.view.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.EditText;

import com.developer.techies.wundartest.R;
import com.developer.techies.wundartest.adapter.CarInfoAdapter;
import com.developer.techies.wundartest.model.CarModel;
import com.developer.techies.wundartest.model.PlaceMarkModel;
import com.developer.techies.wundartest.utils.listener.ItemListener;
import com.developer.techies.wundartest.view.activity.CarInformationActivity;

import java.util.List;

public class CarsInformationFragment extends Fragment implements ItemListener<PlaceMarkModel> {

    RecyclerView mRecyclerView;
    CarInfoAdapter mAdapter;


    List<PlaceMarkModel> placemarks;

    SearchView searchMenu;
    static CarModel mCardModel;

    public CarsInformationFragment() {

    }


    public static CarsInformationFragment newInstance(CarModel carModel) {
        CarsInformationFragment listFragment = new CarsInformationFragment();
        mCardModel = carModel;
        return listFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_car_information, container, false);
        initViews(view);
        initData();
        return view;
    }

    public void initViews(View view) {
        mRecyclerView = view.findViewById(R.id.recycler_view);

        searchMenu = view.findViewById(R.id.search_menu_field);
        searchMenu.setIconifiedByDefault(false);
        searchMenu.setFocusable(true);
        searchMenu.requestFocus();

        int id = searchMenu.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = searchMenu.findViewById(id);
        searchEditText.setHint("Search Addresses");
        searchEditText.setHintTextColor(getResources().getColor(R.color.grey_tint));
    }

    public void initData() {

        if (mCardModel != null)
            placemarks = mCardModel.getPlacemarks();
        mAdapter = new CarInfoAdapter(placemarks);

        mAdapter.attachListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
        initContentData();
    }

    public void initContentData() {
        searchMenu.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText != null)
                    mAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onItemClick(PlaceMarkModel item) {
        Intent intent = new Intent(getActivity(), CarInformationActivity.class);
        intent.putExtra("car_address", item.getAddress());
        intent.putExtra("car_name", item.getName());
        intent.putExtra("car_engine_type", item.getEngineType());
        startActivity(intent);
    }
}
