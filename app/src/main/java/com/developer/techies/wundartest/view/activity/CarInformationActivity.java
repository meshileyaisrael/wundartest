package com.developer.techies.wundartest.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.developer.techies.wundartest.R;

public class CarInformationActivity extends AppCompatActivity {

    TextView carNameTv;
    TextView carEngineTypeTv;
    TextView carAddressTv;

    String carAddress;
    String userName;
    String userReputation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_info);
        setTitle("Car Information");

        initView();
        initData();
    }

    public void initView() {
        carNameTv = findViewById(R.id.car_name);
        carEngineTypeTv = findViewById(R.id.car_engine_type);
        carAddressTv = findViewById(R.id.car_address);
    }

    public void initData() {
        carAddress = getIntent().getStringExtra("car_address");
        userName = getIntent().getStringExtra("car_name");
        userReputation = getIntent().getStringExtra("car_engine_type");

        carNameTv.setText(userName);
        carEngineTypeTv.setText("ENGINE TYPE ==> "+userReputation);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
