package com.developer.techies.wundartest.view.fragment;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.wundartest.R;
import com.developer.techies.wundartest.model.CarModel;
import com.developer.techies.wundartest.model.PlaceMarkModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapInformationFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private GoogleMap googleMap;

    static CarModel mCardModel;
    List<PlaceMarkModel> placemarks;

    private Double mLatitude = 0.00;
    private Double mLongitude = 0.00;

    public MapInformationFragment() {
        // Required empty public constructor
    }

    public static MapInformationFragment newInstance(CarModel carModel) {
        MapInformationFragment listFragment = new MapInformationFragment();
        mCardModel = carModel;
        return listFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_information, container, false);
        initViews();
        return view;
    }

    public void initViews() {
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    public void initData() {
        placemarks = mCardModel.getPlacemarks();

        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);


        for (PlaceMarkModel placemark : placemarks) {
            mLatitude = placemark.getCoordinates().get(0);
            mLongitude = placemark.getCoordinates().get(1);
            String name = placemark.getName();
            MarkerOptions marker = new MarkerOptions().position(new LatLng(mLatitude, mLongitude)).title(name);
            LatLng coordinate = new LatLng(mLatitude, mLongitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 17));
            googleMap.addMarker(marker);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        initData();
    }

    @Override
    public void onLocationChanged(Location location) {
        MarkerOptions marker = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Current Position");
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        LatLng coordinate = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.addMarker(marker);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 17));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
