package com.developer.techies.wundartest.utils;


import com.developer.techies.wundartest.utils.remote.RetrofitClient;
import com.developer.techies.wundartest.utils.remote.Service;

public class ApiUtils {

    public static final String BASE_URL = "https://meshileya.github.io/";

    public static Service getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(Service.class);
    }
}
