package com.developer.techies.wundartest.utils.listener;

public interface ItemListener<T> {
    void onItemClick(T v);
}
