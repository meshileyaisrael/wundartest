package com.developer.techies.wundartest.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.wundartest.R;
import com.developer.techies.wundartest.adapter.holder.CarInfoHolder;
import com.developer.techies.wundartest.model.PlaceMarkModel;
import com.developer.techies.wundartest.utils.listener.ItemListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CarInfoAdapter extends RecyclerView.Adapter<CarInfoHolder>{

    private List<PlaceMarkModel> items = Collections.emptyList();
    private ItemListener<PlaceMarkModel> listener;
    private List<PlaceMarkModel> allItems = new ArrayList<>();

    public void attachListener(ItemListener<PlaceMarkModel> listener){
        this.listener = listener;
    }

    public CarInfoAdapter(List<PlaceMarkModel> items){
        if (items != null) {
            this.allItems.addAll(items);
        }
        this.items = items;
    }

    public void filter(String text) {
        items.clear();

        if (text.isEmpty()) {
            items.addAll(allItems);
        } else {
            text = text.toLowerCase();
            for (PlaceMarkModel placemark : allItems) {
                if (placemark.getAddress().toLowerCase().contains(text) || placemark.getAddress().toLowerCase().contains(text)) {
                    items.add(placemark);
                }
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CarInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_answers, parent, false);
        return new CarInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarInfoHolder holder, int position) {

        PlaceMarkModel item = items.get(position);
        holder.carNameTv.setText(item.getName());
        holder.carAddressTv.setText("Location : "+item.getAddress());
        holder.carFuelTv.setText("Fuel Guage : "+item.getFuel());
        holder.carInteriorTv.setText("Car Interior : "+item.getInterior());
        holder.carExteriorTv.setText("Car Exterior : "+item.getExterior());
        holder.carEngineTypeTv.setText("Engine Type : "+item.getEngineType());
        holder.carVinTv.setText(item.getVin());

        holder.itemView.setOnClickListener(v -> listener.onItemClick(item));
    }

    @Override
    public int getItemCount() {
        return this.items != null ? items.size() : 0;
    }

}
